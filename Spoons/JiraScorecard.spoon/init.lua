--- === JiraScorecard ===
---
--- Check JIRA API periodically and provide a count of current story points for the week
--- in a menubar app. This spoon requires a JIRA API token to be
--- provided to the :start method:
---
--- https://id.atlassian.com/manage-profile/security/api-tokens

-- luacheck: globals hs

local obj = {}

-- Metadata
obj.name = 'JiraScorecard'
obj.version = '1.0'
obj.author = 'Jason Rodriguez <jason@frenteventures.com>'
obj.homepage = 'https://github.com'
obj.license = 'MIT - https://opensource.org/licenses/MIT'

-- Round to x decimal places
local function round(num, numDecimalPlaces)
  local mult = 10^(numDecimalPlaces or 0)
  return math.floor(num * mult + 0.5) / mult
end

-- sets up drop down with extra information
local function setupMenu(blob)
	blob.menu:setMenu({
		{ title = "4 Week:" },
		{ title = "Total: " .. blob.monthPoints, indent = 1 },
		{ title = "Average: " .. blob.monthAverage, indent = 1 }
	})
end

-- update the menu bar
local function updateCount(blob)
	blob.menu:setTitle(blob.displayName .. ': ' .. blob.weekPoints .. ' (' .. blob.weekPotentialPoints .. ')')
	setupMenu(blob)
end


-- process the response
local function onWeekResponse(status, body, headers)
	if status < 0 then
		return
	end

	-- parse json response
	local json = hs.json.decode(body)
	local count = 0
	local userId = ''

	-- loop through issues and add up story points
    for _, issue in pairs(json.issues) do
		count = count + (issue.fields.customfield_10023 or 0)
		userId = issue.fields.assignee.accountId
	end
	if userId ~= '' then
		obj.users[userId].weekPoints = round(count, 2)
		-- update the menu bar
		updateCount(obj.users[userId])
	end
end


-- process the response
local function onPotentialResponse(status, body)
	if status < 0 then
		return
	end

	-- parse json response
	local json = hs.json.decode(body)
	local count = 0
	local userId = ''

	-- loop through issues and add up story points
    for _, issue in pairs(json.issues) do
		count = count + (issue.fields.customfield_10023 or issue.fields.customfield_10015 or 0)
		userId = issue.fields.assignee.accountId
	end

	if userId ~= '' then
		obj.users[userId].weekPotentialPoints = round(count, 2)
		-- update the menu bar
		updateCount(obj.users[userId])
	end
end

-- -- process the response
local function onMonthResponse(status, body)
	if status < 0 then
		return
	end

	-- parse json response
	local json = hs.json.decode(body)
	local count = 0
	local userId = ''

	-- loop through issues and add up story points
    for _, issue in pairs(json.issues) do
		count = count + (issue.fields.customfield_10023 or 0)
		userId = issue.fields.assignee.accountId
	end

	if userId ~= '' then
		obj.users[userId].monthPoints = round(count, 2)
		-- update the menu bar
		setupMenu(obj.users[userId])
	end
end

-- -- process the response
local function onAverageResponse(status, body)
	if status < 0 then
		return
	end

	-- parse json response
	local json = hs.json.decode(body)
	local count = 0
	local userId = ''

	-- loop through issues and add up story points
    for _, issue in pairs(json.issues) do
		count = count + (issue.fields.customfield_10023 or 0)
		userId = issue.fields.assignee.accountId
	end

	if userId ~= '' then
		obj.users[userId].monthAverage = round(count / 4, 2)
		-- update the menu bar
		setupMenu(obj.users[userId])
	end
end

-- timer callback, fetch response
local function onInterval()
	for key, user in pairs(obj.users) do
	hs.http.asyncGet(
  		user.week,
        {
            ["Content-Type"] = "application/json; charset=UTF-8",
			["Authorization"] = "Basic " .. hs.base64.encode("jrodriguez@simplenexus.com:" .. obj.token)
        },
        onWeekResponse
	)
	hs.http.asyncGet(
        user.weekPotential,
        {
            ["Content-Type"] = "application/json; charset=UTF-8",
			["Authorization"] = "Basic " .. hs.base64.encode("jrodriguez@simplenexus.com:" .. obj.token)
        },
        onPotentialResponse
    )
	hs.http.asyncGet(
        user.month,
        {
            ["Content-Type"] = "application/json; charset=UTF-8",
            ["Authorization"] = "Basic " .. hs.base64.encode("jrodriguez@simplenexus.com:" .. obj.token)
        },
        onMonthResponse
	)
	hs.http.asyncGet(
        user.monthAverage,
        {
            ["Content-Type"] = "application/json; charset=UTF-8",
            ["Authorization"] = "Basic " .. hs.base64.encode("jrodriguez@simplenexus.com:" .. obj.token)
        },
        onAverageResponse
    )
	end
end

--- JiraScorecard:start(config)
--- Method
--- Start the spoon
---
--- Parameters:
---  * config - A table containing config values:
--              interval: Interval in seconds to refresh the menu (default 60)
--              token:    JIRA API token (required)
---
--- Returns:
---  * self (allow chaining)
function obj:start(config)
	local interval = config.interval or 300
	self.users = config.users
	self.token = config.token

	for displayName, blob in pairs(self.users) do
		blob['weekPoints'] = 0
		blob['weekPotentialPoints'] = 0
		blob['monthPoints'] = 0
		blob['monthAverage'] = 0
		blob['week'] = 'https://simplenexus.atlassian.net/rest/api/2/search?jql=assignee%3D' .. blob.userId .. '%20AND%20status%3Ddone%20AND%20resolved%20%3E%3D%20startOfWeek%28%29'
		blob['weekPotential'] = 'https://simplenexus.atlassian.net/rest/api/2/search?jql=assignee%3D' .. blob.userId .. '%20AND%20status%20in%20%28%22QA%20Complete%22%2C%20%22Ready%20for%20QA%22%2C%20%22QA%20Testing%22%2C%20%22Ready%20to%20Deploy%22%2C%20%22UX%20Review%22%29'
		blob['month'] = 'https://simplenexus.atlassian.net/rest/api/2/search?jql=assignee%3D' .. blob.userId .. '%20AND%20status%3Ddone%20AND%20resolved%20%3E%3D%20startofWeek%28-4w%29'
		blob['monthAverage'] = 'https://simplenexus.atlassian.net/rest/api/2/search?jql=assignee%3D' .. blob.userId .. '%20AND%20status%3Ddone%20AND%20resolved%20%3E%20startofWeek%28-4w%29%20AND%20resolved%20%3C%20%20endOfWeek%28-1w%29'
		blob['menu'] = hs.menubar.new()
		updateCount(blob)
	end

	-- set timer to fetch periodically
	self.timer = hs.timer.new(interval, onInterval)
	self.timer:start()

	-- -- fetch immediately, too
	onInterval()

	return self
end

--- JiraScorecard:stop()
--- Method
--- Stop the spoon
---
--- Parameters: none
---
--- Returns:
---  * self (allow chaining)
function obj:stop()
	for menu in self.menus do
		menu:removeFromMenuBar()
	end
	self.timer:stop()

	return self
end

return obj