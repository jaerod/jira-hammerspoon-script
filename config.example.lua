-- setup Users you want to track

local userObj = {}
userObj['userId'] = { displayName = 'display_name', userId = 'userId' }
-- i.e.
userObj['5cfa555555555555555555fe'] = { displayName = 'Me', userId = '5cfa555555555555555555fe' }

-- Example Config
local config = {
	-- https://id.atlassian.com/manage-profile/security/api-tokens
    jiraToken = 'TOKEN HERE',
    jiraRefetchInterval = 300, -- This is measured in seconds
    users =  userObj
}

return config